Проект виконаний: Roman Kovalchuk, Igor Bulyshev.

Розподіл завдань:

Завдання №1 - Roman Kovalchuk
(Верстка шапки сайту з верхнім меню (включаючи меню, що випадає при малому розмірі екрану).
Верстка секції People Are Talking About Fork.
Код JavaScript.);

Завдання №2 - Igor Bulishev
(Верстка блоку Revolutionary Editor.
Верстка секції Here is what you get. 
Верстка секції Fork Subscription Pricing.)

При виконанні завдання проекту використані наступні технології:
Gulp,
Sass (Scss),
JavaScript,
Git,
Методологія БЕМ

Проект на GitHub Pages
https://deputatroma.github.io/StepProject-Forkio/



